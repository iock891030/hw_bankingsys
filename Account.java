
/**
 * ***************************************************************
 * CS4001301 Programming Languages * Programming Assignment #1
 *
 * Java programming using subtype, subclass, and exception handling
 *
 * To compile: %> javac Application.java
 *
 * To execute: %> java Application
 *
 *****************************************************************
 */

import java.util.*;

interface TimeConversion {
    long DAY_TO_MSEC = 24 * 60 * 60 * 1000;
    long YEAR_TO_MSEC = 365 * 30 * DAY_TO_MSEC;
    long MONTH_TO_MSEC = 30 * DAY_TO_MSEC;

}

class BankingException extends Exception {

    BankingException() {
        super();
    }

    BankingException(String s) {
        super(s);
    }
}

class reco {
    public Date trancsactionDate;
    public String type;
    public double balance;

    reco(Date transDate, String _type, double Bal) {
        this.trancsactionDate = new Date(transDate.getTime());
        this.type = _type;
        this.balance = Bal;
    }
}

public abstract class Account {

    // protected variables to store commom attributes for every bank accounts
    public String accName;
    public double accBalance;
    public double accInterestRate;
    public Date accOpenDate;
    public Date nextInterestDate;
    public Vector<reco> recos = new Vector<reco>();

    // public methods for every bank accounts
    public String name() {
        return (accName);
    }

    public double balance() {
        return (accBalance);
    }

    public Date getOpenDate() {
        return accOpenDate;
    }

    public double deposit(double amount, Date depositDate) throws BankingException {
        accBalance += amount;
        recos.addElement(new reco(depositDate, "deposit", amount));
        return (accBalance);
    }

    abstract double computeInterest(Date interestDate) throws BankingException;

    public double computeInterest() throws BankingException {
        Date interestDate = new Date();
        return (computeInterest(interestDate));
    }

    public void printInfo() {
        System.out.println(accName + " 餘額 = " + accBalance);
    }

    public void printReco(Date after) {
        System.out.println("於" + after.toString() + "後的交易紀錄:");
        for (reco re : recos) {
            if (re.trancsactionDate.after(after)) {
                System.out.println(re.trancsactionDate.toString() + "," + re.type + ":" + re.balance);
            }
        }
        printInfo();
    }
}

/*

 */
class CheckingAccount extends Account implements TimeConversion {

    private int minBalance = 1000;

    CheckingAccount(String s, double firstDeposit) {
        accName = s;
        accBalance = firstDeposit;
        accOpenDate = new Date();
    }

    CheckingAccount(String s, double firstDeposit, Date firstDate) {
        this(s, firstDeposit);
        accOpenDate = new Date(firstDate.getTime());
    }

    public double withdraw(double amount, Date withdrawDate) throws BankingException {
        // minimum balance is 1000, raise exception if violated
        if ((accBalance - amount) < minBalance) {
            throw new BankingException(accName + "帳戶餘額不足");
        } else {
            accBalance -= amount;
            recos.addElement(new reco(withdrawDate, "withdraw", amount));
            return (accBalance);
        }
    }

    @Override
    public double computeInterest(Date interestDate) throws BankingException {
        throw new BankingException("支票帳戶不計息");
    }
}

/*

 */
class SavingAccount extends Account implements TimeConversion {
    private double fee = 1.0;
    private int feeFreeCount = 3;
    private double accInterestRate = 0.0012;
    private double interest = 0;

    SavingAccount(String name, double firstDeposit) throws BankingException {
        accName = name;
        if (firstDeposit > 0) {
            accBalance = firstDeposit;
        } else {
            throw new BankingException("沒錢還想開戶?" + accName);
        }
        accInterestRate = 0.12;
        nextInterestDate = new Date();
        nextInterestDate.setTime(30 * TimeConversion.DAY_TO_MSEC);
    }

    SavingAccount(String name, double firstDeposit, Date firstDate) throws BankingException {
        accName = name;
        if (firstDeposit > 0) {
            accBalance = firstDeposit;
        } else {
            throw new BankingException("沒錢還想開戶?" + accName);
        }
        nextInterestDate = firstDate;
        recos.addElement(new reco(firstDate, "firstDeposit", firstDeposit));
        nextInterestDate.setTime(30 * TimeConversion.DAY_TO_MSEC);
    }

    public double withdraw(double amount, Date withdrawDate) throws BankingException {
        // no negative balance, raise exception if violated
        if (accBalance < amount) {
            throw new BankingException("沒錢啦 窮鬼" + accName + " ,餘額: " + accBalance);
        } else {
            if (feeFreeCount > 0) {
                feeFreeCount--;
                recos.addElement(new reco(withdrawDate, "withdraw", amount));
            } else {
                recos.addElement(new reco(withdrawDate, "withdraw + fee", amount + fee));
                interest -= fee;
            }
            accBalance -= amount;
            interest -= amount;
            if (interest < 0)
                interest = 0;
            if (accBalance < 0) {
                throw new BankingException("有人把錢領光了" + accName);
            }
            return (accBalance);
        }
    }

    @Override
    public double computeInterest(Date interestDate) throws BankingException {
        if (interestDate.before(nextInterestDate)) {
            throw new BankingException("尚未到計息日" + accName);
        } else {
            interest = (this.accBalance - interest) * this.accInterestRate;
            this.accBalance += interest;
            recos.addElement(new reco(nextInterestDate, "interest:", interest));
            nextInterestDate.setTime(30 + TimeConversion.DAY_TO_MSEC);
        }
        return interest;
    }
}

/*
 *
 */
class CDAccount extends Account implements TimeConversion {
    private Date maturityDate;
    private double feeRate = 0.8;
    private double interest = 0;
    private boolean cleared = false;

    CDAccount(String name, Date openDate, double fixedDeposit, int term) throws BankingException {
        accName = name;
        if (fixedDeposit > 0) {
            accBalance = fixedDeposit;
        } else {
            throw new BankingException("沒錢還想定存? :" + accName);
        }
        maturityDate = new Date();
        maturityDate.setTime(openDate.getTime() + term * TimeConversion.MONTH_TO_MSEC);
        accInterestRate = 0.15;
        nextInterestDate = new Date();
        nextInterestDate.setTime(openDate.getTime() + 30 * TimeConversion.DAY_TO_MSEC);
    }

    // You can't deposit anything and withdrawals cost a $250 fee
    @Override
    public double deposit(double amount, Date depositDate) throws BankingException {
        throw new BankingException("定期戶頭不提供存入:" + accName);
    }

    public double withdraw(Date withdrawDate) throws BankingException {
        if (withdrawDate.before(maturityDate)) {
            while (nextInterestDate.before(withdrawDate))
                computeInterest(maturityDate);
            this.accBalance += this.interest * feeRate - 250;
            recos.addElement(new reco(withdrawDate, "警告，尚未到達約定日，已獲得之利息將打八折，並扣除貳佰伍拾元手續費", this.accBalance));
            recos.addElement(new reco(withdrawDate, "提款", this.accBalance));
            recos.addElement(new reco(withdrawDate, "提前結清銷戶", 0));
            this.cleared = true;
            return this.accBalance;
        } else {
            if (!this.cleared) {
                while (nextInterestDate.before(maturityDate))
                    computeInterest(maturityDate);
                recos.addElement(new reco(withdrawDate, "提款", this.accBalance + this.interest));
                this.cleared = true;
            }
            this.accBalance = 0;
            recos.addElement(new reco(withdrawDate, "已結清", 0));
        }
        return accBalance;
    }

    @Override
    public double computeInterest(Date interestDate) throws BankingException {

        if (interestDate.before(nextInterestDate)) {
            throw new BankingException("尚未到計息日" + accName);
        } else {
            this.interest += (this.accBalance - this.interest) * accInterestRate;
            recos.addElement(new reco(interestDate, " interes ", accBalance + interest));
            nextInterestDate.setTime(nextInterestDate.getTime() + 30 * DAY_TO_MSEC);
        }

        return (accBalance);
    }
}

/*
 * Derived class: LoanAccount
 *
 *
 * 
 */
class LoanAccount extends Account implements TimeConversion {

    boolean cleared;
    private double interest;

    LoanAccount(String name, double loan, Date openDate) throws BankingException {
        accName = name;
        if (loan > 0) {
            accBalance = 0 - loan;
        } else {
            throw new BankingException("你確定你要借錢? " + accName);
        }
        accInterestRate = 0.12;
        nextInterestDate = new Date();
        nextInterestDate.setTime(openDate.getTime() + 30 * TimeConversion.DAY_TO_MSEC);
    }

    @Override
    public double deposit(double amount, Date depositDate) throws BankingException {
        if (amount < 0)
            throw new BankingException("你確定你有要還錢?" + accName);
        else if (cleared) {
            throw new BankingException("借錢戶已結清" + accName);
        } else {
            accBalance += amount;
            recos.addElement(new reco(depositDate, "deposit:", this.accBalance));
            while (nextInterestDate.before(depositDate))
                computeInterest(depositDate);
        }

        if (accBalance > 0)
            cleared = true;

        return accBalance;
    }

    public double withdraw(double amount, Date withdrawDate) throws BankingException {
        throw new BankingException("借錢不還 再借困難" + accName);
    }

    @Override
    public double computeInterest(Date interestDate) throws BankingException {
        if (interestDate.before(nextInterestDate))
            throw new BankingException("尚未達計息日" + accName);
        if (cleared)
            throw new BankingException("該戶頭已結清" + accName);

        interest = this.accBalance * accInterestRate;
        this.accBalance += interest;
        recos.addElement(new reco(nextInterestDate, "interest", this.accBalance));
        nextInterestDate.setTime(nextInterestDate.getTime() + 30 * DAY_TO_MSEC);
        return accBalance;
    }
}
