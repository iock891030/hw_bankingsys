
/**
 * ***************************************************************
 * CS4001301 Programming Languages * Programming Assignment #1
 *
 * Java programming using subtype, subclass, and exception handling
 *
 * To compile: %> javac Application.java
 *
 * To execute: %> java Application
 *
 *****************************************************************
 */
import java.util.*;

public class Application implements TimeConversion {

    public static void main(String args[]) throws BankingException {

        /* testing checking account */
        System.out.println("====================================================================================");
        Date fakeDate = new Date();
        CheckingAccount testCheckingAcc = new CheckingAccount("testCheckingAcc", 10000, fakeDate);
        try {
            Date recoDate = new Date(fakeDate.getTime());
            System.out
                    .println("=============================test Checking Account====================================");
            testCheckingAcc.printInfo();
            fakeDate.setTime(fakeDate.getTime() + 1 * DAY_TO_MSEC);
            System.out.println(testCheckingAcc.name() + " deposit " + 500 + " at " + fakeDate.toString());
            testCheckingAcc.deposit(500, fakeDate);
            testCheckingAcc.printInfo();
            fakeDate.setTime(fakeDate.getTime() + 1 * DAY_TO_MSEC);
            System.out.println(testCheckingAcc.name() + " withdraw " + 1000 + " at " + fakeDate.toString());
            testCheckingAcc.withdraw(1000, fakeDate);
            testCheckingAcc.printInfo();
            System.out.println(testCheckingAcc.name() + " printReco after " + recoDate.toString());
            testCheckingAcc.printReco(recoDate);
            fakeDate.setTime(fakeDate.getTime() + 30 * DAY_TO_MSEC);
            testCheckingAcc.computeInterest(fakeDate);
        } catch (Exception e) {
            stdExceptionPrinting(e, testCheckingAcc.balance());
        }

        /* testing saving account */
        System.out.println("====================================================================================");
        Date fdate = new Date();
        SavingAccount testSavingingAcc = new SavingAccount("testSavingAcc", 10000, fakeDate);
        try {
            Date recoDate = new Date(fdate.getTime());
            System.out.println("=============================test Saving Account====================================");
            testSavingingAcc.printInfo();
            fdate.setTime(fdate.getTime() + 1 * DAY_TO_MSEC);
            System.out.println(testSavingingAcc.name() + " deposit " + 500 + " at " + fdate.toString());
            testSavingingAcc.deposit(500, fdate);
            testSavingingAcc.printInfo();
            fdate.setTime(fdate.getTime() + 1 * DAY_TO_MSEC);
            System.out.println(testSavingingAcc.name() + " withdraw " + 1000 + " at " + fdate.toString());
            testSavingingAcc.withdraw(1000, fdate);
            testSavingingAcc.withdraw(1000, fdate);
            testSavingingAcc.withdraw(1000, fdate);
            testSavingingAcc.withdraw(1000, fdate);
            testSavingingAcc.printInfo();
            fdate.setTime(fdate.getTime() + 30 * DAY_TO_MSEC);
            testSavingingAcc.computeInterest(fdate);
            System.out.println(testSavingingAcc.name() + " printReco after " + recoDate.toString());
            testSavingingAcc.printReco(recoDate);

        } catch (Exception e) {
            stdExceptionPrinting(e, testCheckingAcc.balance());
        }
        /* testing CD account */
        System.out.println("====================================================================================");
        fakeDate = new Date();
        System.out.println(fakeDate.toString());
        CDAccount testCDAcc = new CDAccount("testCDAcc", fakeDate, 10000, 6);

        try {
            Date recoDate = new Date(fakeDate.getTime() - 1000);
            System.out.println("=============================test CD Account====================================");
            testCDAcc.printInfo();
            fakeDate.setTime(fakeDate.getTime() + 1000);
            // testCDAcc.deposit(1000, fakeDate);
            fakeDate.setTime(fakeDate.getTime() + 3 * 30 * DAY_TO_MSEC + 1000);
            testCDAcc.withdraw(fakeDate);
            testCDAcc.printReco(recoDate);
        } catch (Exception e) {
            stdExceptionPrinting(e, testCheckingAcc.balance());
        }
        /* testing loan account */
        System.out.println("====================================================================================");
        fakeDate = new Date();
        System.out.println(fakeDate.toString());
        LoanAccount testLoanAcc = new LoanAccount("testLoanAcc", 10000, fakeDate);

        try {
            Date recoDate = new Date(fakeDate.getTime() - 1000);
            System.out.println("=============================test loan Account====================================");
            testLoanAcc.printInfo();
            fakeDate.setTime(fakeDate.getTime() + 1000);
            // testCDAcc.deposit(1000, fakeDate);
            testLoanAcc.deposit(1000, fakeDate);
            fakeDate.setTime(fakeDate.getTime() + 3 * 30 * DAY_TO_MSEC + 1000);
            testLoanAcc.deposit(1000, fakeDate);
            testLoanAcc.printReco(recoDate);
        } catch (Exception e) {
            stdExceptionPrinting(e, testCheckingAcc.balance());
        }

    }

    static void stdExceptionPrinting(Exception e, double balance) {
        System.out.println("EXCEPTION: Banking system throws a " + e.getClass() + " with message: \n\t" + "MESSAGE: "
                + e.getMessage());
        System.out.println("\tAccount balance remains $" + balance + "\n");
    }
}
